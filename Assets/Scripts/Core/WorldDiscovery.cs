﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Core
{
    class WorldDiscovery : MonoBehaviour
    {
        public IWorldManipulation World;
        public bool Active = false;
        public int HorizontalChunkRadius = 2;
        private float elapsedTime = 0;

        void Update()
        {
            elapsedTime += Time.deltaTime;
            if (elapsedTime > 1)
            {
                World.RequestTerrainAround(transform.position, HorizontalChunkRadius);
            }
        }
    }
}
