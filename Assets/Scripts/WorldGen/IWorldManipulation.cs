﻿using System.Collections.Generic;
using UnityEngine;

public interface IWorldManipulation
{
    void LoadChunk(Vector3Int pos);
    void UnloadChunk(Vector3Int pos);
    IList<int> ResolveUpmostChunksPos(int absXpos, int absZpos);
    void RequestTerrainAround(Vector3 position, int horizontalChunkRadius);
}