using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Core.Interaction;
using Assets.Scripts.Helpers;
using MeshGen;
using MeshGen.WorldGen;
using UnityEngine;
using static Assets.Scripts.Helpers.MathHelpers;

public class WorldMapBehavior : MonoBehaviour, IWorldManipulation
{
	public GameObject ChunkPrefab;

	IMapProvider mainMap;
	public const int ChunkSize = 8;
	public Dictionary<Vector3Int, Chunk> Chunks { get; set; }
	public Dictionary<Vector3Int, ChunkBehaviour> LoadedChunkObjects { get; set; }

    private GameObject LastClickedGizmo;

    void Awake() 
	{
		Chunks = new Dictionary<Vector3Int, Chunk>();
        LoadedChunkObjects = new Dictionary<Vector3Int, ChunkBehaviour>();
		mainMap = InstanceFactory.GetInstance().GetMapProvider(); 
	}
	void Start() {
        for (int y = -3; y < 2; y++)
        {
            for (int x = -3; x < 3; x++)
            {
                for (int z = -3; z < 3; z++)
                {
                    LoadChunk(new Vector3Int(x, y, z));
                }
            }
        }
    }

	public void LoadChunk(Vector3Int pos) {
		GameObject go = Instantiate(ChunkPrefab, pos * ChunkSize, transform.rotation);
        ChunkBehaviour chunkBehavior = go.GetComponent<ChunkBehaviour>();
        
        chunkBehavior.Interacted += ChunkBehavior_Interacted;

		if (Chunks.ContainsKey(pos)) { //get from recycled
			Chunk tchunk = Chunks[pos];
            chunkBehavior.MapChunk = tchunk;
		}
		else
		{
            chunkBehavior.MapChunk = mainMap.GetChunk(pos, ChunkSize);
            Chunks.Add(pos, chunkBehavior.MapChunk);
            go.name = pos.ToString();
            LoadedChunkObjects.Add(pos, chunkBehavior);
        }
	}
    public void UnloadChunk(Vector3Int pos)
    {
        throw new NotImplementedException();
    }
    
    private void ChunkBehavior_Interacted(RaycastHit hit, InteractionType interaction)
    {
        switch (interaction)
        {
            case InteractionType.Destroy:
                Vector3 respos = (hit.point - hit.normal / 2);
                Vector3Int hitPosInsideBlock = new Vector3Int((int)Mathf.Floor(respos.x), (int)Mathf.Floor(respos.y), (int)Mathf.Floor(respos.z));
                var chunkpos = GetChunkPos(hitPosInsideBlock);

                SetBlock(hitPosInsideBlock, new MeshGen.WorldGen.Space());
                GetChunkBehaviour(chunkpos).RegenerateMesh();
                break;
        }
    }

    private ChunkBehaviour GetChunkBehaviour(Vector3Int chunkPos)
    {
        return LoadedChunkObjects[chunkPos];
    }
    public Block GetBlock(Vector3Int absPos)
    {
        var chunk = LoadedChunkObjects[GetChunkPos(absPos)].MapChunk.GetBlockArray();

        Vector3Int localPos = GetBlockLocalPos(absPos);
        try
        {
            return chunk[localPos.x, localPos.y, localPos.z];
        }
        catch (IndexOutOfRangeException)
        {
            print(localPos.ToString());
        }
        return null;
    }
    public void SetBlock(Vector3Int absPos, Block block)
    {
        Vector3Int localPos = GetBlockLocalPos(absPos);
        //Debug.Log("zmieniono blok na pozycji: " + localPos.ToString());

        LoadedChunkObjects[GetChunkPos(absPos)].MapChunk.SetBlock(localPos, block);
    }

    public IList<int> ResolveUpmostChunksPos(int absXpos, int absZpos)
    {
        var normalizedChunkPos = GetChunkPos(new Vector3Int(absXpos, 0, absZpos));
        List<int> positions = new List<int>();
        for (int x = 0; x < ChunkSize; x++)
        {
            int maxHeight = (int)mainMap.GetPerlinHeight(normalizedChunkPos.x + 0, normalizedChunkPos.z + 0);
            int minHeight = (int)mainMap.GetPerlinHeight(normalizedChunkPos.x + 0, normalizedChunkPos.z + 0);
            for (int z = 0; z < ChunkSize; z++)
            {
                int h = (int)mainMap.GetPerlinHeight(normalizedChunkPos.x + x, normalizedChunkPos.z + z);
                if (h > maxHeight)
                {
                    maxHeight = h;
                }
                if (h < minHeight)
                {
                    minHeight = h;
                }
                maxHeight = NormalizeValue(maxHeight);
                minHeight = NormalizeValue(minHeight);
                for (int i = minHeight; i <= maxHeight; i++)
                {
                    positions.Add(i);
                }
            }
        }
        return positions;
    }

    private Vector3Int GetBlockLocalPos(Vector3Int absPos)
    {
        int x = Modulo(absPos.x, ChunkSize);
        int y = Modulo(absPos.y, ChunkSize);
        int z = Modulo(absPos.z, ChunkSize);
        return new Vector3Int(x, y, z);
    }
    private Vector3Int GetChunkPos(Vector3Int absPos)
    {
        Vector3Int chunkPos = new Vector3Int
        {
            x = (int)Mathf.Floor(absPos.x / (float)ChunkSize),
            y = (int)Mathf.Floor(absPos.y / (float)ChunkSize),
            z = (int)Mathf.Floor(absPos.z / (float)ChunkSize)
        };
        return chunkPos;
    }

    private int NormalizeValue(int value)
    {
        return (int)Mathf.Floor(value / (float)ChunkSize);
    }

    public void RequestTerrainAround(Vector3 position, int horizontalChunkRadius)
    {
        for (int i = -horizontalChunkRadius; i < horizontalChunkRadius * 2; i++)
        {
            //LoadChunk(new Vector3Int())
        }
    }
}