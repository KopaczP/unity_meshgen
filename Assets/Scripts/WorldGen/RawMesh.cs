﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RawMesh {
    public Vector3[] Vertices;
    public int[] Triangles;
    public Vector2[] Uv;

    public RawMesh()
    {
        Vertices = new Vector3[0];
        Triangles = new int[0];
        Uv = new Vector2[0];
    }
}
