using MeshGen.WorldGen;
using System.Collections.Generic;
using UnityEngine;

public interface IMapProvider {
    Block GetBlock(Vector3 posAbs);
    Chunk GetChunk(Vector3 posAbs, int chunkSize);
    float GetPerlinHeight(int absXPos, int absZPos);
}