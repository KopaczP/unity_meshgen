﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GMRepo : MonoBehaviour {
    public GameObject WorldMapPrefab;
    public GameObject PlayerPrefab;

    private IWorldManipulation World;
    private GameObject Player;
    
    void Start () {
        var worldObject = Instantiate(WorldMapPrefab);
        worldObject.transform.position = new Vector3(0, 0, 0);
        World = worldObject.GetComponent<IWorldManipulation>();
        var positions = World.ResolveUpmostChunksPos(0, 0);
        foreach (var pos in positions)
        {
            World.LoadChunk(new Vector3Int(0, pos, 0));
        }
        Player = Instantiate(PlayerPrefab);
        Player.transform.position = new Vector3(4, positions.Max() * 8 + 12, 4);
    }
}
